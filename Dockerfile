# ---- Base python ----
FROM python:3.9.8-slim
WORKDIR /app
COPY . /app

ARG PKGTIME

RUN apt-get update \
    && apt-get install -y --no-install-recommends unzip \
    && apt-get autoremove -yqq --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Record the backup name (with date) in a file
RUN basename -s .zip pkgs/Inventory_$PKGTIME.zip > /app/webinventory/static/info/backup

# Extract inventory backup and mv files in the app
RUN unzip /app/pkgs/Inventory_$PKGTIME.zip -d /app/pkgs \
    && mkdir -p /app/webinventory/static/[info|imgs] \
    && mv /app/pkgs/*.jpg /app/webinventory/static/imgs/ \
    && mv /app/pkgs/data.xml /app/webinventory/static/info/ \
    && mv /app/pkgs/data.xml.xslt /app/webinventory/static/info/ \
    && mv /app/pkgs/inventory.csv /app/webinventory/static/info/ \
    && rm -rf /app/pkgs/*

RUN pip install -U pip
RUN pip install -r requirements.txt

CMD ["gunicorn", "--config", "./conf/gunicorn_config.py", "wsgi:app"]
