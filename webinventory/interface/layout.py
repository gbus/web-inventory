import dash_bootstrap_components as dbc
from dash import dcc
from dash import html


def mk_look_inside_button(button_text: str) -> html:
    return html.Span([html.I(className="bi bi-box-arrow-right"), " ", button_text])


def get_cards(inventory):

    idata = inventory.get_item()

    cards = []
    if idata:
        for invx, invd in enumerate(idata["children"]):
            card = dbc.Card(
                [
                    dbc.CardImg(src=f"/static/imgs/{invd['@image']}", top=True),
                    dbc.CardBody(
                        [
                            html.H4(
                                dbc.Button(
                                    mk_look_inside_button(invd["@name"]),
                                    id={'type': 'controls', 'index': invx},
                                    outline=True,
                                    color="primary"
                                ),
                                className="card-title"
                            ),
                            html.P(
                                invd["description"],
                                className="card-text",
                            ),
                        ]
                    ),
                ],
                style={"width": "18rem"},
            )
            cards.append(card)
    return cards


def get_head(inventory):
    idata = inventory.get_item()

    return html.Div(
        dbc.Container(
            [
                html.H1([
                    html.I(className="bi bi-box-seam"),
                    " ",
                    idata["@name"]
                ], className="display-3"),
                html.P(
                    idata["description"],
                    className="lead",
                ),
                html.Hr(className="my-2"),
                html.P([
                    dbc.Button(
                        html.I(className="bi bi-house-fill"),
                        id={'type': 'controls', 'index': -100},
                        outline=True,
                        color="primary"
                    ),
                    dbc.Button(
                        [html.I(className="bi bi-box-arrow-left"), " ", "Back"],
                        id={'type': 'controls', 'index': -1},
                        outline=True,
                        color="primary"
                    ),
                    # dbc.Button("Home(-100)", id={'type': 'controls', 'index': -100}, color="primary"),
                    # dbc.Button("Back(-1)", id={'type': 'controls', 'index': -1}, color="primary"),
                ], className="lead"),
            ],
            fluid=True,
            className="py-3",
        ),
        className="p-3 bg-light rounded-3",
    )


def get_main(inventory):
    main_page = dbc.Container([
        dbc.Row([
            dbc.Col(get_head(inventory), id='pagehead', width=12)
        ]),
        dbc.Row(get_cards(inventory),  id='pagecards', justify="center"),
    ])
    return main_page
