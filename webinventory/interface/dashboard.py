from dash import Dash, Input, Output, ALL
from dash import ctx
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc
from webinventory.parser import load_inventory, InventoryParser
from webinventory.interface.layout import get_main, get_head, get_cards


def init_callbacks(app, inventory):

    @app.callback(
        Output('pagehead', 'children'),
        Output('pagecards', 'children'),
        [Input({'type': 'controls', 'index': ALL}, 'n_clicks')],
        prevent_initial_call=True
    )
    def control_responder(gonext):
        if gonext is None:
            raise PreventUpdate
        else:
            button_clicked = ctx.triggered_id
            print(button_clicked)
            if button_clicked:
                if button_clicked["index"] >= 0:
                    inventory.set_next(button_clicked["index"])
                elif button_clicked["index"] == -1:
                    inventory.set_prev()
                elif button_clicked["index"] == -100:
                    inventory.reset()
            return get_head(inventory), get_cards(inventory)


def init_data():
    idata = load_inventory()
    inventory = InventoryParser(idata)
    return inventory


def init_dashboard(server):
    """Create a Plotly Dash dashboard."""
    dash_app = Dash(
        server=server,
        routes_pathname_prefix='/dashapp/',
        external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.BOOTSTRAP]
    )

    inventory = init_data()
    # inventory.set_next(0)

    # Create Dash Layout
    dash_app.layout = get_main(inventory)

    # Initialize callbacks after our app is loaded
    # Pass dash_app as a parameter
    init_callbacks(dash_app, inventory)

    return dash_app.server
