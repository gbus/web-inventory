import pydash
import xmltodict

DATA_FILE = "webinventory/static/info/data.xml"


def load_inventory():
    with open(DATA_FILE, "r") as inventory_file:
        inventory_data = xmltodict.parse(inventory_file.read())
    return inventory_data


class InventoryParser:
    BASE_LOCATION = ["inventory"]
    location = []

    def __init__(self, data):
        self.data = data
        self.reset()

    def _get_data_at_current_location(self):
        return pydash.get(self.data, self.location)

    def get_children(self):
        curr_data = self._get_data_at_current_location()
        children_label = self._get_children_label()
        children_data = []
        if children_label in curr_data.keys():
            children_data = curr_data[children_label]

        children = []
        for item in children_data:
            children.append(
                self._get_current_content(item)
            )
        return children

    def get_item(self):
        curr_data = self._get_data_at_current_location()
        item = self._get_current_content(curr_data)
        item["children"] = self.get_children()

        return item

    def set_next(self, next_location_index):
        if len(self.location) == 2:
            self.location.append(next_location_index)
        else:
            self.location.append(self._get_children_label())
            self.location.append(next_location_index)

    def set_prev(self):
        if len(self.location) > 3:
            self.location.pop()
            self.location.pop()

    def reset(self):
        self.location = self.BASE_LOCATION.copy()

    def _get_children_label(self):
        last_label = self.location[-1]  # last location item can be a string (property, room, item, etc)
        if isinstance(last_label, int):
            last_label = self.location[-2]  # last location item can be an id number, refer to the previous one

        if last_label == "inventory":
            return "property"
        elif last_label == "property":
            return "room"
        elif last_label == "room":
            return "item"
        else:
            return "item"

    @staticmethod
    def _get_current_content(item):
        return {
            "@id": item["@id"] if "@id" in item.keys() else "NA",
            "@name": item["@name"] if "@name" in item.keys() else "",
            "@image": item["@image"] if "@image" in item.keys() else "",
            "description": item["description"] if "description" in item.keys() else "",
        }

    def _get_child_index(self, child_id: str):
        return [i for i, _ in enumerate(self._get_data_at_current_location()) if _['@id'] == child_id][0]
